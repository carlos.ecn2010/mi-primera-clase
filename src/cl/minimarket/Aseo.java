
package cl.minimarket;


public class Aseo extends Producto{
    protected String categoria;

    public Aseo() {
    }

    public Aseo(String categoria, int codigo, int precio, int cantidad, String descripcion) {
        super(codigo, precio, cantidad, descripcion);
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

        @Override
    public int total(){
        int total = 0;
        total = this.precio * this.cantidad;
        return total;
    }
    
    @Override
    public String toString() {
        return super.toString()+ ", categoria: " + categoria + ", total: " + total();
    }
    
    
}
