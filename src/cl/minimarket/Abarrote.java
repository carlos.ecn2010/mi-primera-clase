
package cl.minimarket;


public class Abarrote extends Producto{
    //peso lo vamos a manejar gramos
    protected int gramos;

    public Abarrote() {
    }

    public Abarrote(int gramos, int codigo, int precio, int cantidad, String descripcion) {
        super(codigo, precio, cantidad, descripcion);
        this.gramos = gramos;
    }

    public int getGramos() {
        return gramos;
    }

    public void setGramos(int gramos) {
        this.gramos = gramos;
    }

    @Override
    public int total(){
        int total = 0;
        total = this.precio * this.cantidad;
        return total;
    }
    
    
    @Override
    public String toString() {
        return super.toString() + ", gramos: " + gramos + ", total: " + total();
    }
    
    
    
}
