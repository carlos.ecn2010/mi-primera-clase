
package cl.minimarket;

public class Oferta extends Producto{
    protected boolean aplica;

    public Oferta() {
    }

    public Oferta(boolean aplica, int codigo, int precio, int cantidad, String descripcion) {
        super(codigo, precio, cantidad, descripcion);
        this.aplica = aplica;
    }

    public boolean isAplica() {
        return aplica;
    }

    public void setAplica(boolean aplica) {
        this.aplica = aplica;
    }

    @Override
    public int total(){
        int total = 0;
        total = this.precio * this.cantidad;
        return total;
    }    
    
    @Override
    public String toString() {
        return super.toString()+ ", aplica oferta: " + aplica + ", total: " + total();
    }
    
    
}
