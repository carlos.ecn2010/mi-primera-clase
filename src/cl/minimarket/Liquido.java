
package cl.minimarket;


public class Liquido extends Producto{
    protected boolean alcohol;

    public Liquido() {
    }

    public Liquido(boolean alcohol, int codigo, int precio, int cantidad, String descripcion) {
        super(codigo, precio, cantidad, descripcion);
        this.alcohol = alcohol;
    }

    public boolean isAlcohol() {
        return alcohol;
    }

    public void setAlcohol(boolean alcohol) {
        this.alcohol = alcohol;
    }

    @Override
    public int total(){
        int total = 0;
        total = this.precio * this.cantidad;
        return total;
    }    
    
    @Override
    public String toString() {
        return super.toString() + ", alcohol: " + alcohol + ", total: " + total();
    }

    
    
    
    
}
