/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.minimarket;

/**
 *
 * @author CETECOM
 */
public class Congelado extends Producto {

    protected String tipo;

    public Congelado() {
    }

    public Congelado(String tipo, int codigo, int precio, int cantidad, String descripcion) {
        super(codigo, precio, cantidad, descripcion);
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public int total() {
        int total = 0;
        total = this.precio * this.cantidad;
        return total;
    }

    @Override
    public String toString() {
        return super.toString() + ", tipo: " + tipo + ", total: " + total();
    }

}
