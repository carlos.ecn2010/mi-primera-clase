
package cl.minimarket;

import java.util.ArrayList;


public class Lista {
    ArrayList<Producto> listaProd = new ArrayList<>();

    public Lista() {
    }
    
    public void agregar(Producto prod){
        listaProd.add(prod);
    }
    
    public void eliminar(Producto prod){
        listaProd.remove(prod);
    }
    
    public void listar(){
        for(Producto temp: listaProd){
            System.out.println(temp);
        }
    }
    
    public int totalMax(){
        int totalMax = 0;
        for(Producto temp : listaProd){
            totalMax = totalMax + temp.total();
        }
        return totalMax;
    }
    
    
    
}
